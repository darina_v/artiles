﻿using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Collections;

public static class AwaitExtensions
{
    public static TaskAwaiter GetAwaiter(this TimeSpan timeSpan)
    {
        return Task.Delay(timeSpan).GetAwaiter();
    }
}

public enum ScreenshotFormat
{
    PNG, JPEG, EXR
}

public class NativeShare : MonoBehaviour
{
    public event Action OnScreenshotCaptured;
    public string subject, ShareMessage, url;
    private bool isProcessing = false;
    string filePath;

    public async Task CaptureScreenshotAsync(ScreenshotFormat screenshotFormat = ScreenshotFormat.PNG)
    {
        await new WaitForEndOfFrame();
        name = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
        filePath = Path.Combine(Application.persistentDataPath, name);

        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();
        byte[] bytes = null;

        switch (screenshotFormat)
        {
            case ScreenshotFormat.PNG:
                bytes = screenImage.EncodeToPNG();
                break;
            case ScreenshotFormat.JPEG:
                bytes = screenImage.EncodeToJPG();
                break;
            case ScreenshotFormat.EXR:
                bytes = screenImage.EncodeToEXR();
                break;
            default:
                break;
        }
        
        Debug.Log("Saved Data to: " + filePath.Replace("/", "\\"));
        await WriteBytesAsync(filePath, bytes);
        OnScreenshotCaptured?.Invoke();
    }

    private async Task WriteBytesAsync(string path, byte[] bytes)
    {
        using (FileStream sourceStream = new FileStream(path,
            FileMode.Append, FileAccess.Write, FileShare.None,
            bufferSize: 4096, useAsync: true))
        {
            await sourceStream.WriteAsync(bytes, 0, bytes.Length);
        };
    }

    public async void Share()
    {
#if UNITY_ANDROID
        if (!isProcessing)
            await ShareScreenshot();
#elif UNITY_IOS
        if(!isProcessing)
            CallSocialShareAdvanced("", "", "", filePath);
#else
            Debug.Log("No sharing set up for this platform.");
#endif
    }

#if UNITY_ANDROID
    async Task ShareScreenshot()
    {
        isProcessing = true;
        await new WaitForEndOfFrame();

        if (!Application.isEditor)
        {
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaClass fileProviderClass = new AndroidJavaClass("android.support.v4.content.FileProvider");
            AndroidJavaObject javaFileObj = new AndroidJavaObject("java.io.File", filePath);
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
            AndroidJavaObject contextObject = activity.Call<AndroidJavaObject>("getApplicationContext");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
            string autoritiesString = contextObject.Call<AndroidJavaObject>("getApplicationContext").Call<string>("getPackageName") + ".fileprovider";
            AndroidJavaObject uriObject = fileProviderClass.CallStatic<AndroidJavaObject>("getUriForFile", contextObject, autoritiesString, javaFileObj);
            intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            intentObject.Call<AndroidJavaObject>("setType", "image/*");
            AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Share Via");
            activity.Call("startActivity", jChooser);
        }
        isProcessing = false;
    }
#endif
#if UNITY_IOS
    public struct ConfigStruct
    {
        public string title;
        public string message;
    }

    [DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);
    public struct SocialSharingStruct
    {
        public string text;
        public string url;
        public string image;
        public string subject;
    }

    [DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);
    public void CallSocialShare(string title, string message)
    {
        isProcessing = true;
        ConfigStruct conf = new ConfigStruct();
        conf.title = title;
        conf.message = message;
        showAlertMessage(ref conf);
        isProcessing = false;
    }

    public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
    {
        SocialSharingStruct conf = new SocialSharingStruct();
        conf.text = defaultTxt; 
        conf.url = url;
        conf.image = img;
        conf.subject = subject;
        showSocialSharing(ref conf);
    }
#endif    
}