﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MarkerController : MonoBehaviour
{
    public GameObject markerPrefab;
    public float createHeight;

    List<GameObject> markers;

    [SerializeField]
    Canvas canvas;

    public event System.Action OnFirstMarkerPlaced;
    public event System.Action OnLastMarkerRemoved;

    public List<Vector2> MarkerLocations
    {
        get { return markers.Select((m => new Vector2(m.transform.position.x, m.transform.position.z))).ToList(); }
    }

    void Start ()
    {
        markers = new List<GameObject>();
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (UIController.IsPointerOverUI())
            {
                return;
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;
            int layerMask = 1 << LayerMask.NameToLayer("ARGameObject"); // Planes are in layer ARGameObject
            if (Physics.Raycast(ray, out rayHit, float.MaxValue, layerMask))
            {
                Vector3 position = rayHit.point;
                CreateMarker(new Vector3(position.x, position.y + createHeight, position.z));
            }
        }
    }

    void CreateMarker(Vector3 atPosition)
    {
        if(markers.Count == 0)
        {
            OnFirstMarkerPlaced();
        }

        GameObject markerGO = Instantiate(markerPrefab, atPosition, Quaternion.identity, transform);
        markers.Add(markerGO);
    }

    public void RemoveLastMarker()
    {
        if(markers.Count == 0)
        {
            return;
        }
        Destroy(markers[markers.Count - 1]);
        markers.RemoveAt(markers.Count - 1);

        if (markers.Count == 0)
        {
            OnLastMarkerRemoved();
        }
    }

    public void ResetMarkers()
    {
        foreach (var m in markers)
        {
            Destroy(m);
        }
        markers.Clear();
        OnLastMarkerRemoved();
    }

    void OnGUI()
    {
        if (markers.Count > 0)
        {
            //foreach (var item in markers)
            //{

            //}
            //var m = markers.Select(l => l.transform.position.y).ToList();
            //GUI.Label(new Rect(100, 100, 600, 300), System.String.Format("Markers: {0}, {1},{2},{3},{4},{5}", m[0], m[1], m[2], m[3], m[4], m[5]));
        }
    }
}
