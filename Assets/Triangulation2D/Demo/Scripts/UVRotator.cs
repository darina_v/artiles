﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVRotator : MonoBehaviour
{
    public float rotateSpeed = 30f;
    public void Update()
    {
        Vector2 halfVector = new Vector2(0.5f, 0.5f);
        var uvs = GetComponent<MeshFilter>().mesh.uv;
        
        // Construct a rotation matrix and set it for the shader
        Quaternion rot = Quaternion.Euler(0, 0, Time.time * rotateSpeed);
        Matrix4x4 m = Matrix4x4.TRS(transform.position, rot, Vector3.one);
        GetComponent<Renderer>().material.SetMatrix("_TextureRotation", m);
        
    }
}
