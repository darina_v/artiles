﻿using System;
using UnityEngine;

public interface IGameController
{
    void BeginMarkerPlacing();
    void InitializeMarkerPlacing();
    void RemoveLastMarker();
    void ResetMarkers();
    void BuildMesh();
    void RebuildMesh();
    void ApproveMeshTransform();
    void CompleteTexturePicking();
    void ResetSession();

    event Action OnSessionReset;

    void CaptureScreenshotAsync();
    void ShareScreenshot();
}

public class GameController : MonoBehaviour, IGameController
{
    [SerializeField]
    MarkerController markerController;

    [SerializeField]
    MeshBuilder meshBuilder;

    [SerializeField]
    IUIController uiController;

    bool meshMovementAvailable;
    [SerializeField]
    NativeShare shareN;

    public event Action OnSessionReset;

    void Start()
    {
        uiController = FindObjectOfType<UIController>();
        markerController.gameObject.SetActive(false);
        markerController.OnFirstMarkerPlaced += uiController.EnableRemoveLast;
        markerController.OnLastMarkerRemoved += uiController.DisableRemoveLast;
        shareN.OnScreenshotCaptured += EnableSharing;
    }
    
    public void BeginMarkerPlacing()
    {
        uiController.StartSession();
    }

    public void InitializeMarkerPlacing()
    {
        markerController.gameObject.SetActive(true);
        uiController.EnableMarkerPlacing();
    }

    public void RemoveLastMarker()
    {
        markerController.RemoveLastMarker();
    }

    public void ResetMarkers()
    {
        markerController.ResetMarkers();
    }

    public void BuildMesh()
    {
        if(markerController.MarkerLocations.Count < 3)
        {
            return;
        }
        meshBuilder.Build(markerController.MarkerLocations);
        markerController.ResetMarkers();
        markerController.gameObject.SetActive(false);
        uiController.DisableMarkerPlacing();
        uiController.ApproveTextureStep(true);
        meshMovementAvailable = true;
    }

    void Update()
    {
        if (meshMovementAvailable && Input.GetMouseButton(0))
        {
            if (UIController.IsPointerOverUI())
            {
                return;
            }
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit;
            int layerMask = 1 << LayerMask.NameToLayer("ARGameObject"); // Planes are in layer ARGameObject
            if (Physics.Raycast(ray, out rayHit, layerMask))
            {
                Debug.DrawRay(ray.origin, ray.direction * 100, Color.yellow);
                Vector3 position = rayHit.point;
                meshBuilder.ReturnMeshTransform().position = new Vector3(position.x, position.y + 0.01f, position.z);
                var q = meshBuilder.ReturnMeshTransform().position;
            }
        }
    }

    public void RebuildMesh()
    {
        meshMovementAvailable = false;
        markerController.gameObject.SetActive(true);
        meshBuilder.DeleteLastMesh();
        uiController.EnableMarkerPlacing();
        uiController.ApproveTextureStep(false);
    }

    public void ApproveMeshTransform()
    {
        meshMovementAvailable = false;
    }

    public void CompleteTexturePicking()
    {

    }

    public void ResetSession()
    {
        meshBuilder.DeleteLastMesh();
        OnSessionReset?.Invoke();
    }    

    public async void CaptureScreenshotAsync()
    {
        await shareN.CaptureScreenshotAsync();
    }

    public void ShareScreenshot()
    {
        shareN.Share();
    }

    private void EnableSharing()
    {
        uiController.EnableSharing();
    }
}


