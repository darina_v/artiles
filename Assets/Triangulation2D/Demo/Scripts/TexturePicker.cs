﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ITexturePicker
{
    void SetMaterial(int index);
}

public class TexturePicker : MonoBehaviour, ITexturePicker
{
    [SerializeField]
    Button buttonPrefab;
    [SerializeField]
    Material[] availableMaterials;
    [SerializeField]
    Sprite[] materialIcons;
    [SerializeField]
    GameObject ButtonsGrid;
    [SerializeField]
    MeshBuilder meshBuilder;

    List<Button> textureButtons;

    void Awake()
    {
        textureButtons = new List<Button>(ButtonsGrid.GetComponentsInChildren<Button>());
    }

    void Start()
    {
        SetupButtons();
    }

    void SetupButtons()
    {
        for (int i = 0; i < availableMaterials.Length; i++)
        {
            var a = i;
            if (textureButtons.Count <= i)
            {
                var go = Instantiate(buttonPrefab, ButtonsGrid.transform);
                textureButtons.Add(go);
            }
            var tex = textureButtons[i].GetComponentsInChildren<Image>();
            tex[1].sprite = materialIcons[a];
            //textureButtons[i].GetComponentInChildren<Text>().text = string.Format("Material #{0}", a);
            textureButtons[i].onClick.AddListener(() => SetMaterial(a));
        }
    }

    public void SetMaterial(int index)
    {
        meshBuilder.ReturnLastMesh().SetMaterial(availableMaterials[index]);
    }
}
