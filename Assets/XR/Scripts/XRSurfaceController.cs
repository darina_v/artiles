using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class XRSurfaceController : MonoBehaviour {

  private const int STARTING_DISTANCE = 10000;
  private const float GROUND_DISTANCE_GUESS = 1.2f; // ~4ft
  private const float MAX_GROUND_HEIGHT = 0.3f;

  // If true, XRSurfaceController will update the rendered mesh and the collider mesh of the surface
  // so that it matches the detected surface. This allows for interactions like shadows that clip
  // to surface boundaries, and objects that can fall off surfaces.
  public bool deformToSurface = false;

  // If true, game object will appear as placed in the scene prior to surface detection.
  // If false, game object will be moved out of view until surface is detected.
  // When surface is detected, object will be moved to surface position.
  public bool displayImmediately = false;

  // If true, only place on "ground" surfaces (< MAX_GROUND_HEIGHT)
  // If false, use any detected surface.
  public bool groundOnly = false;

  // If true, place on first detected surface and don't move
  // If false, move game object to active surface
  public bool lockToFirstSurface = true;

  private XRController xr;
  private bool surfaceFound = false;
  private float groundHeightGuess;

  private MeshFilter meshFilter = null;
  private MeshCollider meshCollider = null;

  private long surfaceId = Int64.MinValue;
  Dictionary<long, Vector3> centerMap;

  private bool initialized = false;
  private Quaternion originalRotation_;

  // If true, this gameObject will be rotated with the normal of the detected surface.
  private bool rotatePlaneWithDetectedNormal = true;

  void Start() {
    originalRotation_ = transform.localRotation;
    centerMap = new Dictionary<long, Vector3>();
    xr = GameObject.FindWithTag("XRController").GetComponent<XRController>();
    if (!xr.DisabledInEditor()) {
      Initialize();
    }
  }

  private void Initialize() {
    initialized = true;

    // Add MeshFilter and MeshCollider if not already added.
    meshFilter = gameObject.GetComponent<MeshFilter>();
    if (deformToSurface && meshFilter == null) {
      meshFilter = gameObject.AddComponent<MeshFilter>();
      meshFilter.mesh = new Mesh();
    }

    meshCollider = gameObject.GetComponent<MeshCollider>();
    if (deformToSurface && meshCollider == null) {
      meshCollider = gameObject.AddComponent<MeshCollider>();
      meshCollider.sharedMesh = meshFilter.mesh;
    }

    if (groundOnly) {
      // Start by moving object a reasonable guess for the ground height.
      groundHeightGuess = Camera.main.transform.position.y - GROUND_DISTANCE_GUESS;
      SetHeight(groundHeightGuess);
    }

    if (!displayImmediately && !xr.GetCapabilities().IsSurfaceEstimationFixedSurfaces()) {
      // Move surface very far in the distance until there is a detected surface.
      transform.position = new Vector3(STARTING_DISTANCE, transform.position.y, STARTING_DISTANCE);
    }

    if (xr.GetCapabilities().IsSurfaceEstimationFixedSurfaces() && lockToFirstSurface) {
      surfaceFound = true;
    }
  }

  private void SetHeight(float h) {
    transform.position = new Vector3 (transform.position.x, h, transform.position.z);
  }

  private Vector3 GetVertexCenter(Mesh mesh) {
    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

    foreach (Vector3 vertex in mesh.vertices) {
      x += vertex.x;
      y += vertex.y;
      z += vertex.z;
    }
    double il = 1.0 / mesh.vertices.Length;

    return new Vector3((float)(x * il), (float)(y * il), (float)(z * il));
  }

  private void UpdateSurface(long id, XRSurface surface) {
    // If looking at a different surface than before
    bool surfaceSelected = false;
    if (id != surfaceId) {
      Vector3 vertexCenter = GetVertexCenter(surface.mesh);

      if (groundOnly) {
        float y = vertexCenter.y;

        // Check to see if surface can be considered ground
        if (y < (groundHeightGuess + MAX_GROUND_HEIGHT)
          && surface.type != XRSurface.Type.VERTICAL_PLANE) {
          if (!centerMap.ContainsKey(id)) {
            centerMap.Add(id, vertexCenter);
          }
          if (!displayImmediately && !surfaceFound) {
            transform.position = centerMap[id];
          }

          if (lockToFirstSurface) {
            // Don't move, just lower to the ground surface
            SetHeight(y);
          } else {
            transform.position = centerMap[id];
          }
          surfaceFound = true;
          surfaceSelected = true;
        }
      } else {
        if (!centerMap.ContainsKey(id)) {
          centerMap.Add(id, vertexCenter);
        }

        transform.position = centerMap[id];
        surfaceId = id;
        surfaceFound = true;
        surfaceSelected = true;
      }
    }

    if (surfaceSelected && rotatePlaneWithDetectedNormal) {
      transform.localRotation = originalRotation_;
      transform.Rotate(surface.rotation.eulerAngles);
    }

  }

  void DeformMesh(Mesh mesh) {
    // Set the mesh vertices relative to the transform center.
    Vector3[] relVertices = new Vector3[mesh.vertices.Length];
    Vector3 anchor = transform.position;
    for (int i = 0; i < mesh.vertices.Length; ++i) {
      relVertices[i] = new Vector3(
        mesh.vertices[i].x - anchor.x,
        mesh.vertices[i].y - anchor.y,
        mesh.vertices[i].z - anchor.z);
    }

    // The number of vertices change so we need to create a new mesh
    meshFilter.sharedMesh.Clear();
    meshFilter.sharedMesh.vertices = relVertices;
    meshFilter.sharedMesh.normals = mesh.normals;
    meshFilter.sharedMesh.uv = mesh.uv;
    meshFilter.sharedMesh.triangles = mesh.triangles;
  }

  void Update() {
    if (xr.DisabledInEditor()) {
      return;
    }

    if (!initialized) {
      Initialize();
    }

    XRSurface surface = !lockToFirstSurface || surfaceId == Int64.MinValue
      ? xr.GetSurface(xr.GetActiveSurfaceId()) : xr.GetSurface(surfaceId);
    // If no mesh, reset the id to default and don't change anything.
    if (surface == XRSurface.NO_SURFACE || surface.mesh == null) {
      surfaceId = Int64.MinValue;
      return;
    }
    if (!lockToFirstSurface || !surfaceFound) {
      UpdateSurface(xr.GetActiveSurfaceId(), surface);
    }

    if (deformToSurface && !xr.GetCapabilities().IsSurfaceEstimationFixedSurfaces()) {
      DeformMesh(surface.mesh);
    }
  }
}
