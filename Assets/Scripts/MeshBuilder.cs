﻿using UnityEngine;
using System.Collections.Generic;
using mattatz.Triangulation2DSystem;
using mattatz.Triangulation2DSystem.Example;

public class MeshBuilder : MonoBehaviour 
{
    [SerializeField, Range(10f, 30f)] float angle = 20f;
    //[SerializeField, Range(0.1f, 2f)] float threshold = 1.5f;
    [SerializeField] GameObject meshPrefab;
    [SerializeField] Transform testPlane;
    [SerializeField] Transform meshParent;

    GameObject floorMesh;

    public void Build (List<Vector2> points) 
    {
        if (points.Count <= 2)
        {
            return;
        }
        
        //points = Utils2D.Constrain(points, threshold);
        var polygon = Polygon2D.Contour(points.ToArray());

        var vertices = polygon.Vertices;
        if(vertices.Length < 3) return; // error

        var triangulation = new Triangulation2D(polygon, angle);
        //var position = new Vector3(testPlane.position.x, testPlane.position.y + 0.01f, testPlane.position.z);
        var go = Instantiate(meshPrefab);
        go.transform.position = new Vector3(go.transform.position.x, testPlane.position.y + 0.01f, go.transform.position.z);
        var mesh = go.GetComponent<DemoMesh>();
        mesh.SetTriangulation(triangulation);
        mesh.SetAreaText();
        meshParent.transform.position = mesh.transform.TransformPoint(mesh.GetComponent<MeshFilter>().mesh.bounds.center);// + (Vector3.up * 0.01f);
        mesh.transform.SetParent(meshParent);
        mesh.transform.localPosition = new Vector3(mesh.transform.localPosition.x, 0, mesh.transform.localPosition.z);
        floorMesh = go;
    }

    public void DeleteLastMesh()
    {
        Destroy(floorMesh);
    }

    public DemoMesh ReturnLastMesh()
    {
        return floorMesh.GetComponent<DemoMesh>();
    }

    public Transform ReturnMeshTransform()
    {
        return meshParent;
    }
}

