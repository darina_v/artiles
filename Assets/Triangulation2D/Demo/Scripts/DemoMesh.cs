﻿using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

using System.Collections;
using System.Collections.Generic;
using System;

namespace mattatz.Triangulation2DSystem.Example
{
	[RequireComponent (typeof(MeshFilter))]
	public class DemoMesh : MonoBehaviour 
    {
        public float area;
        [SerializeField] TextMesh areaText;
        [SerializeField] Material lineMat;

        Triangle2D[] triangles;

		public void SetTriangulation (Triangulation2D triangulation) 
        {
            var mesh = triangulation.Build();
            GetComponent<MeshFilter>().mesh = mesh;

            area = triangulation.CalculateMeshArea();
            triangles = triangulation.Triangles;
            mesh.RecalculateNormals();
            RecalculateUV();
            mesh.RecalculateBounds();
        }

        public void SetAreaText()
        {
            areaText.text = area.ToString("F2") + "sq.m";
            areaText.transform.localPosition = GetComponent<MeshFilter>().mesh.bounds.center;
        }

        public void SetMaterial(Material mat)
        {
            MeshRenderer rend = GetComponent<MeshRenderer>();
            rend.material = mat;
        }

        void RecalculateUV()
        {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;
            Vector2[] uvs = new Vector2[vertices.Length];

            uvs = Array.ConvertAll(vertices, element => new Vector2(element.x, element.y));
            mesh.uv = uvs;
            RenderObject();
        }

        void RenderObject() 
        {
			if(triangles == null) return;
			GL.PushMatrix();
			GL.MultMatrix (transform.localToWorldMatrix);

            if (lineMat != null)
            {
                lineMat.SetColor("_Color", Color.black);
                lineMat.SetPass(0);
            }

            GL.Begin(GL.LINES);
            for (int i = 0, n = triangles.Length; i < n; i++)
            {
                var t = triangles[i];
                GL.Vertex(t.s0.a.Coordinate); GL.Vertex(t.s0.b.Coordinate);
                GL.Vertex(t.s1.a.Coordinate); GL.Vertex(t.s1.b.Coordinate);
                GL.Vertex(t.s2.a.Coordinate); GL.Vertex(t.s2.b.Coordinate);
            }
            GL.End();
            GL.PopMatrix();
        }
    }
}

