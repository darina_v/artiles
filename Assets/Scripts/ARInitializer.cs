﻿using UnityEngine;

namespace CrossPlatformAR
{
    public class ARInitializer : MonoBehaviour
    {
        [SerializeField]
        IGameController gc;
        [SerializeField]
        GameObject testPlane;
        [SerializeField]
        XRController xrc;

        bool surfaceFound;

        //GUIStyle guiStyle = new GUIStyle();
        
        void OnEnable()
        {
            gc = FindObjectOfType<GameController>();
        }

        void Start()
        {
            testPlane.SetActive(false);
            gc.OnSessionReset += ResetSession;

            if (!UniAndroidPermission.IsPermitted(AndroidPermission.CAMERA))
            {
                UniAndroidPermission.RequestPermission(AndroidPermission.CAMERA);
            }
        }

        void Update()
        {
            if(surfaceFound)
            {
                return;
            }
#if UNITY_EDITOR
            surfaceFound = true;
            OnSurfaceFound();
            return;
#endif
            if (xrc.GetSurfaces().Count > 0)
            {
                OnSurfaceFound();
            }
        }
        
        void OnSurfaceFound()
        {
            surfaceFound = true;
            testPlane.SetActive(true);
            //xrc.GetSurfaces()[0]
            gc.BeginMarkerPlacing();
        }

//#if UNITY_EDITOR
//        void OnGUI()
//        {
//            guiStyle.fontSize = 80;
//            GUI.Label(new Rect(200, 200, 600, 300), String.Format("Plane on: {0}", testPlane.transform.position.y));
//        }
//#endif

        void ResetSession()
        {
            gc.BeginMarkerPlacing();
        }
    }
}
