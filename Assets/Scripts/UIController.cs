﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface IUIController
{
    void StartSession();
    void EnableMarkerPlacing();
    void DisableMarkerPlacing();
    void ApproveTextureStep(bool enable);
    void EnableRemoveLast();
    void DisableRemoveLast();
    void EnableSharing();
}

public class UIController : MonoBehaviour, IUIController
{
    [SerializeField] IGameController gc;
    //0
    [SerializeField] GameObject scanHint;
    //1
    [SerializeField] Button removeLastMarkerButton;
    [SerializeField] Button resetMarkersButton;
    [SerializeField] Button buildMesh;
    //2
    [SerializeField] Button rebuildMesh;
    [SerializeField] Button showTexturePicker;
    //3
    [SerializeField] Button completeTextureSelectionButton;
    //4
    [SerializeField] Button resetButton;
    [SerializeField] Button photoButton;
    [SerializeField] Button shareButton;
    [SerializeField] Button placeOrderButton;

    [SerializeField] GameObject hint;
    [SerializeField] TexturePicker texturePicker;

    public static bool IsPointerOverUI()
    {
        bool isoverUI = false;
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);
        if (raycastResults.Count > 0)
        {
            foreach (var res in raycastResults)
            {
                isoverUI |= (res.gameObject.GetComponent<UIBehaviour>() != null);
            }
        }
        return isoverUI;
    }

    private void Start()
    {
        gc = FindObjectOfType<GameController>();

        hint.GetComponentInChildren<Button>().onClick.AddListener(OnHintOKPressed);
        
        removeLastMarkerButton.onClick.AddListener(gc.RemoveLastMarker);
        resetMarkersButton.onClick.AddListener(gc.ResetMarkers);
        buildMesh.onClick.AddListener(gc.BuildMesh);
        rebuildMesh.onClick.AddListener(gc.RebuildMesh);
        showTexturePicker.onClick.AddListener(ShowTexturePicker);
        completeTextureSelectionButton.onClick.AddListener(CompleteTextureSelection);
        //TODO: placeOrderButton.onClick.AddListener(     
        resetButton.onClick.AddListener(ResetSession);
        photoButton.onClick.AddListener(gc.CaptureScreenshotAsync);
        shareButton.onClick.AddListener(gc.ShareScreenshot);

        Initialize();        
    }

    public void Initialize()
    {
        removeLastMarkerButton.gameObject.SetActive(false);
        resetMarkersButton.gameObject.SetActive(false);
        buildMesh.gameObject.SetActive(false);
        rebuildMesh.gameObject.SetActive(false);
        showTexturePicker.gameObject.SetActive(false);
        completeTextureSelectionButton.gameObject.SetActive(false);
        placeOrderButton.gameObject.SetActive(false);
        texturePicker.gameObject.SetActive(false);
        resetButton.gameObject.SetActive(false);
        photoButton.gameObject.SetActive(false);
        shareButton.gameObject.SetActive(false);
        hint.SetActive(false);
        scanHint.SetActive(true);
        DisableRemoveLast();
    }

    public void StartSession()//
    {
        scanHint.SetActive(false);
        hint.SetActive(true);
    }

    public void OnHintOKPressed()//
    {
        hint.SetActive(false);
        gc.InitializeMarkerPlacing();
    }

    public void EnableMarkerPlacing()//
    {
        hint.SetActive(false);
        removeLastMarkerButton.gameObject.SetActive(true);
        resetMarkersButton.gameObject.SetActive(true);
        buildMesh.gameObject.SetActive(true);
    }

    public void EnableRemoveLast()
    {
        removeLastMarkerButton.interactable = true;
        var color = removeLastMarkerButton.GetComponentInChildren<Text>().color;
        color.a = 1f;
        removeLastMarkerButton.GetComponentInChildren<Text>().color = color;
    }

    public void DisableRemoveLast()
    {
        removeLastMarkerButton.interactable = false;
        var color = removeLastMarkerButton.GetComponentInChildren<Text>().color;
        color.a = .3f;
        removeLastMarkerButton.GetComponentInChildren<Text>().color = color;
    }

    public void DisableMarkerPlacing()
    {
        removeLastMarkerButton.gameObject.SetActive(false);
        resetMarkersButton.gameObject.SetActive(false);
        buildMesh.gameObject.SetActive(false);
    }

    public void ApproveTextureStep(bool enable)
    {
        rebuildMesh.gameObject.SetActive(enable);
        showTexturePicker.gameObject.SetActive(enable);
    }

    public void ShowTexturePicker()
    {
        gc.ApproveMeshTransform();
        texturePicker.gameObject.SetActive(true);
        ApproveTextureStep(false);
        completeTextureSelectionButton.gameObject.SetActive(true);
    }

    void CompleteTextureSelection()
    {
        //texturePicker.gameObject.SetActive(false);
        completeTextureSelectionButton.gameObject.SetActive(false);
        //placeOrderButton.gameObject.SetActive(true);
        resetButton.gameObject.SetActive(true);
        photoButton.gameObject.SetActive(true);
        shareButton.gameObject.SetActive(true);
        shareButton.interactable = false;
    }

    public void ShowPlaceOrderButton()
    {
        //TODO: lalala
    }

    public void ResetSession()
    {
        Initialize();
        gc.ResetSession();
    }

    public void EnableSharing()
    {
        shareButton.interactable = true;
    }
}
